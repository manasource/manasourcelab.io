---
layout: post
title: The Mana World Organization rebranded to Manasource
author: Thorbjørn Lindeijer
---

Back in 2009 the Mana project [split off from The Mana World]({{ site.baseurl
}}{% post_url news/2009-10-24-the-mana-project-started %}) in the hopes of
encouraging collaboration and to shield developers from community politics.

Unfortunately, with many developers moving on, the Mana project became dormant
sometime after the [Mana 0.6.1 release]({{ site.baseurl }}{% post_url
news/2012-04-08-mana-client-0.6.1-released %}). Some games that relied on this
client became inactive as well, but [The Mana
World](https://www.themanaworld.org/) instead relied on the ManaPlus fork and is
still alive today.

The Mana World is not only alive, but evolved from a single game into an
organization that develops and maintains multiple closely related games. It now
develops [Moubootaur Legends](https://moubootaurlegends.org/) and [Source of
Mana](https://the-mana-world.itch.io/source-of-mana) as well, both based on
different servers and the latter also based on a new client.

To avoid confusion and to make each of these games feel at home within the
shared organization, The Mana World Organization is now rebranding to Manasource
and uses the previously dorment `manasource.org` domain as its new home!
