---
layout: default
title: Support
---

# Support

## Where to report bugs

If you hit any bugs, want to suggest any features or simply want to ask any
question please open an issue on the bugtracker of the relevant project at one
of the following locations:

* [https://git.themanaworld.org](https://git.themanaworld.org)
* [https://gitlab.com/manasource](https://gitlab.com/manasource)
* [https://github.com/mana](https://github.com/mana)

If you report a bug please make sure to give as much information as possible
(such as used version, used operating system, ...)

## Old bugtracker (currently unavailable)

Before we switched to github we used
[https://bugs.manasource.org/](http://bugs.manasource.org/) for reporting.
So if you want to look through old bugs or feature suggestions feel free to
check there.
