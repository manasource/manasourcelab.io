---
layout: default
title: Development
---

# Development

## Contact us

Find us on IRC at <b>irc.libera.chat</b> in the following channels:

* \#themanaworld
* \#themanaworld-dev
* \#sourceofmana
* \#moubootaur-legends
* \#mana

You can also visit and register on our <a href="https://forums.themanaworld.org/viewforum.php?f=31">Forums</a>.

We're also on <a href="https://discord.gg/jhpDNAakdk">Discord</a> where our main channels are bridged to IRC. Check out our <a href="https://wiki.themanaworld.org/wiki/The_Mana_World:Chat">chat directory</a> for more info and options to talk to us.

## Git repositories

The Mana World and client projects: <a href="https://git.themanaworld.org/explore/groups">git.themanaworld.org</a>.

Source of Mana: <a href="https://gitlab.com/sourceofmana">gitlab.com/sourceofmana</a>.

Moubootaur Legends: <a href="https://git.themanaworld.org/ml">git.themanaworld.org/ml</a>.

Mana: <a href="https://gitlab.com/manasource">gitlab.com/manasource</a> and <a href="https://git.themanaworld.org/mana/mana">git.themanaworld.org/mana/mana</a>.
